<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Http\Request;
use App\Models\User;

class RoleController extends Controller
{
    public function addRole(Request $request) {
        try {
            $validateRole = Validator::make($request->all(), 
                [
                    'role' => 'required'
            ]);
        
            if($validateRole->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateRole->errors()
                ], 401);
            }

            $role = Role::create(['name' => $request->role]);
            return response()->json(['message' => "Role $role->name create succefully"]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 400);
        }
    }

    // Add role for a user
    public function userRole(Request $request, $id) {
        try {
            $validateRole = Validator::make($request->all(), 
                [
                    'role' => 'required'
            ]);
        
            if($validateRole->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateRole->errors()
                ], 401);
            }
            $user = User::find($id);
            $role = Role::findByName($request->role, 'sanctum');
            $user->assignRole($role);

            return response()->json([
                'message' => $user->getRoleNames()
            ]);

        } catch(\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 400);
        }
    }

    // remove role for a user
    public function removeUserRole(Request $request, $id) {
        try {
            $validateRole = Validator::make($request->all(), 
            [
                'role' => 'required'
            ]);
        
            if($validateRole->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateRole->errors()
                ], 401);
            }
            $user = User::find($id);
            $role = Role::findByName($request->role, 'sanctum');
            $user->removeRole($role);

            return response()->json([
                'message' => $user->getRoleNames()
            ]);

        } catch(\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 400);
        }
    }

    // remove role for a user
    public function deleteRole(Request $request) {
        try {
            $validateRole = Validator::make($request->all(), 
            [
                'role' => 'required'
            ]);
        
            if($validateRole->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateRole->errors()
                ], 401);
            }
            $role = Role::findByName($request->role, 'sanctum');
            $role->delete();

            return response()->json([
                'message' => "Role succefully delete"
            ]);

        } catch(\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 400);
        }
    }
}
