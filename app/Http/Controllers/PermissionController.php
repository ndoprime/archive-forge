<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Http\Request;
use App\Models\User;

class PermissionController extends Controller
{

    /**
     * Create Permission
     * @return Permission
     */
    public function createPermission(Request $request) {

        try {

            $validatePermission = Validator::make($request->all(), 
                [
                    'permission' => 'required'
            ]);
        
            if($validatePermission->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validatePermission->errors()
                ], 401);
            }

            $permission = Permission::create(['name' => $request->permission]);

            return response()->json(['message' => "Permission $permission->name create succefully"]);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 400);
        }
    }

    /**
     * Add Permission in role
     * @return Permission
     */
    public function addPermissionToRole(Request $request) {
        try {
            $validatePermission = Validator::make($request->all(), 
                [
                    'permission' => 'required',
                    'role' => 'required'
            ]);
        
            if($validatePermission->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validatePermission->errors()
                ], 401);
            }

            $role = Role::findByName($request->role, 'sanctum');
            $role->givePermissionTo($request->permission);
            return response()->json([
                'message' => $request->permission
            ]);
        } catch(\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 400);
        }
    }

    /**
     * Remove Permission in role
     * @return Permission
     */
    public function removePermissionToRole(Request $request) {
        try {
            $validatePermission = Validator::make($request->all(), 
                [
                    'permission' => 'required',
                    'role' => 'required'
            ]);
        
            if($validatePermission->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validatePermission->errors()
                ], 401);
            }

            $role = Role::findByName($request->role, 'sanctum');
            $role->revokePermissionTo($request->permission);
            return response()->json([
                'message' => $request->permission
            ]);
        } catch(\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 400);
        }
    }

    /**
     * Add Direct Permission in user
     * @return Permission
     */
    public function addPermissionToUser(Request $request, $id) {
        try {
            $validatePermission = Validator::make($request->all(), 
                [
                    'permission' => 'required'
            ]);
        
            if($validatePermission->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validatePermission->errors()
                ], 401);
            }

            $user = User::find($id);
            $user->givePermissionTo($request->permission);
            return response()->json([
                'message' => $user->getRoleNames()
            ]);
        } catch(\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 400);
        }
    }

    /**
     * Remove Direct Permission in user
     * @return Permission
     */
    public function removePermissionToUser(Request $request, $id) {
        try {
            $validatePermission = Validator::make($request->all(), 
                [
                    'permission' => 'required'
            ]);
        
            if($validatePermission->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validatePermission->errors()
                ], 401);
            }

            $user = User::find($id);
            $user->revokePermissionTo($request->permission);
            return response()->json([
                'message' => $request->permission
            ]);
        } catch(\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 400);
        }
    }

    /**
     * Delete Permission
     * @return Permission
     */
    public function deletePermission(Request $request) {
        try {
            $validatePermission = Validator::make($request->all(), 
            [
                'permission' => 'required'
            ]);
        
            if($validatePermission->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validatePermission->errors()
                ], 401);
            }
            $permission = Permission::findByName($request->permission, 'sanctum');
            $permission->delete();

            return response()->json([
                'message' => "Permission succefully delete"
            ]);

        } catch(\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 400);
        }
    }
}
