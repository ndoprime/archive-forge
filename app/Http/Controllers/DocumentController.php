<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use App\Models\Document;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Resources\DocumentResource;
use App\Http\Resources\DocumentCollection;

use Illuminate\Support\Facades\File;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new DocumentCollection(Document::all());
    }

    /**
     * Store a newly created resource in storage. glpat-VRZ6VaCkWs6d5Gxvr2zc
     */
    public function store(Request $request)
    {
        try {
            // $validateRole = Validator::make($request->all(), 
            //     [
            //         'file' => 'required'
            // ]);
        
            // if($validateRole->fails()){
            //     return response()->json([
            //         'status' => false,
            //         'message' => 'validation error',
            //         'errors' => $validateRole->errors()
            //     ], 401);
            // }

            // $document = Document::create([
            //     'name' => $request->file('file')->getClientOriginalName(), 
            //     'path' => $request->file('file')->path(),
            //     'extension' =>  $request->file('file')->extension(),
            //     'user_id' => User::Find(1)->id, 
            //     'document_id' => Document::find(3)->id
            // ]);

            // $request->file('file')->store('files');
  
            // Display the list of all file
            // and directory
            return "test";
            // return response()->json(['message' => number_format($request->file('file')->getSize() / 1024, 2)." KB" ]);
            
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Document $document)
    {
        return new DocumentResource($document);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Document $document)
    {
        try {
            $validateRole = Validator::make($request->all(), 
                [
                    'name' => 'required',
                    'extension' => 'required'
            ]);
        
            if($validateRole->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateRole->errors()
                ], 401);
            }

            $document->name = $request->name;
            $document->extension = $request->extension;

            $document->save();
            // Storage::put('cities.csv', 'C:\Users\AUGENTIC PASSCAM\Documents\cities.csv');

            return response()->json(['message' => "Documente update succefully"]);
            
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Document $document)
    {
        try {
            $document->delete();
            return response()->json(['message' => "Document delete succefully"]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 400);
        }
    }
}
