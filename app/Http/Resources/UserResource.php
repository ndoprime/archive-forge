<?php

namespace App\Http\Resources;

use App\Http\Resources\PermissionResource;
use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\User;
 
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            "role" => $this->getRoleNames(),
            "permissions" => new PermissionCollection($this->getAllPermissions()),
            "document" => $this->document,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
