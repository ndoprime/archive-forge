<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\DocumentController;

use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Http\Resources\RoleCollection;
use App\Http\Resources\RoleResource;

use App\Models\User;
use App\Models\Document;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
| Invitation Postman API link : https://app.getpostman.com/join-team?invite_code=3c9c7960311ee2978cfd29152fe8757a
*/

// Authentication
Route::post('/auth/register', [AuthController::class, 'createUser'])->name('register');
Route::post('/auth/login', [AuthController::class, 'loginUser'])->name('login');

// Document operations
Route::post('/document', [DocumentController::class, 'store'])->name('store.document');
Route::get('/documents', [DocumentController::class, 'index'])->name('index.doxument');
Route::get('/document/{document}', [DocumentController::class, 'show'])->name('show.document');
Route::delete('/document/{document}', [DocumentController::class, 'destroy'])->name('delete.document');
Route::put('/document/{document}', [DocumentController::class, 'update'])->name('update.document'); // ?


Route::middleware(['auth:sanctum'])->group(function () {
    // Need to be authenticated
    Route::get('/me', function (Request $request) {
        return new UserResource($request->user());
    });
    Route::get('/users', function (Request $request) {
        return new UserCollection(User::all());
    });
    Route::get('/user/{id}', function ($id) {
        return new UserResource(User::findOrFail($id));
    });
    Route::post('/auth/change-password', [AuthController::class, 'password.update']);

    //  Role operations
    Route::post('/user_role/{id}', [RoleController::class,'userRole'])->name('user.role');
    Route::delete('/user_role/{id}', [RoleController::class,'removeUserRole'])->name('remove.user.role');
    Route::post('/role', [RoleController::class, 'addRole'])->name('add.role');
    Route::delete('/role', [RoleController::class, 'deleteRole'])->name('delete.role');
    Route::get('/roles', function() { return Role::all(); });

    // Permission operations
    Route::post('/permission', [PermissionController::class, 'createPermission'])->name('create.permission');
    Route::delete('/permission', [PermissionController::class, 'deletePermission'])->name('permission.delete');
    Route::post('/role_permission', [PermissionController::class, 'addPermissionToRole'])->name('add.role.permission');
    Route::delete('/role_permission', [PermissionController::class, 'removePermissionToRole'])->name('remove.role.permission');
    Route::post('/user_permission/{id}', [PermissionController::class, 'addPermissionToUser'])->name('add.user.permission');
    Route::delete('/user_permission/{id}', [PermissionController::class, 'removePermissionToUser'])->name('remove.user.permission');
    Route::get('/permissions', function() { return Permission::all(); });

    

});
